package com.sip.ams.Controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sip.ams.users.users;

@Controller
@RequestMapping("home")
public class FirstController {
	@RequestMapping(value = "/home")
	//@ResponseBody
	public String home(Model model) {
		String name = "hassen" ;
		String formation ="Full-Stack";
		model.addAttribute("name", name);
		model.addAttribute("formation", "formation");
		
		ArrayList<String> listUsers = new ArrayList<>();	
		ArrayList<users> candidats = new ArrayList<users>();
		
		listUsers.add("hassen");
		listUsers.add("kamel");
		listUsers.add("ridha");
		listUsers.add("hanen");
		model.addAttribute("list", listUsers);
		model.addAttribute("listNb",listUsers.size());
		
	candidats.add(new users("hassen", "ben hamida", "hassen28255296@gmail.com"));
	candidats.add(new users("wissam", "ben salem", "wissalbenSalem@gmail.com"));
	model.addAttribute("candidats", candidats);

		
		
		return "first/home";
	}

}
